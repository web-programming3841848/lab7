type UtilityDetial = {
    id: number,
    type: string,
    price: number,
}
export type { UtilityDetial }