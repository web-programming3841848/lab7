type Category = "drink" | "bakery" | "foods";
type SubCategory = "Hot" | "Cold" | "Frappe";
type SweetLevel = "0" | "25" | "50" | "100";
type Size = "S" | "M" | "L";


type Product = {
  id: number;
  name: string;
  price: number;
  unit: number;
  category: Category;
  subCategory?: SubCategory;
  sweetLevel?: SweetLevel;
  size?: Size;
};

export type { Category, SubCategory, SweetLevel, Product, Size };