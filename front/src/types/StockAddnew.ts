type StockForSend = {
  date: string
  userID: number
}
export type { StockForSend }
