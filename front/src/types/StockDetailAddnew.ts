type StockDetailForSend = {
  QOH: number
  balance: number
  materialId: number
  StockId: number
}
export type { StockDetailForSend }
