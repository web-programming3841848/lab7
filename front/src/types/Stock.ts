type Stock = {
  id: number
  date: string
  userID: number
}

export type { Stock }
