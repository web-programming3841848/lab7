import http from './http'

type ReturnData = {
  celsius: number
  fahrenhiet: number
}

async function convert(celsius: number): Promise<number> {
  console.log(`/temparature/convert/${celsius}`)
  const res = await http.post(`/temparature/convert`, {
    celsius: celsius
  })
  const convertResult = res.data as ReturnData
  return convertResult.fahrenhiet
}

export default { convert }
