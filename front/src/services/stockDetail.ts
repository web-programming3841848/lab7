import type { StockDetail } from '../types/StockDetail'
import type { StockDetailForSend } from '@/types/StockDetailAddnew'
import http from './http'

function addStockDetail(stockDetail: StockDetailForSend) {
  return http.post('/stocksDetail', stockDetail)
}
function updateStockDetail(stockDetail: StockDetail) {
  return http.patch(`/stocksDetail/${stockDetail.id}`, stockDetail)
}
function delStockDetail(stockDetail: StockDetail) {
  return http.delete(`/stocksDetail/${stockDetail.id}`)
}
function getStockDetail(id: number) {
  return http.get(`/stocksDetail/${id}`)
}
function getStockDetails() {
  return http.get('/stocksDetail')
}
function getStockDetailsByStockId(StockId: number) {
  return http.get(`/stocksDetail/${StockId}`)
}

export default {
  addStockDetail,
  updateStockDetail,
  delStockDetail,
  getStockDetail,
  getStockDetails,
  getStockDetailsByStockId
}
