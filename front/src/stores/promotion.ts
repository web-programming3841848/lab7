import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/Promotion'

export const usePromotionStore = defineStore('promotion', () => {
  const promotionDialog = ref(false)
  const promotions = ref<Promotion[]>([
        {
            id: 1,
            name: "Summer",
            startDate: new Date('2023-12-27'),
            endDate: new Date('2024-01-14'),
            detail: ["drink"],
            status: "Active",
            discount: 10
          },
          {
            id: 2,
            name: "Winter Combo",
            startDate: new Date('2024-01-15'),
            endDate: new Date('2024-02-01'),
            detail: ["bakery", "foods"],
            status: "Active",
            discount: 5
          },
          {
            id: 3,
            name: "Year-End Feast",
            startDate: new Date('2024-12-01'),
            endDate: new Date('2024-12-31'),
            detail: ["drink", "bakery", "foods"],
            status: "Inactive",
            discount: 15
          },
          {
            id: 4,
            name: "Spring Sips",
            startDate: new Date('2024-03-01'),
            endDate: new Date('2024-03-15'),
            detail: ["drink"],
            status: "Active",
            discount: 5
          },
          {
            id: 5,
            name: "Easter Delights",
            startDate: new Date('2024-04-01'),
            endDate: new Date('2024-04-10'),
            detail: ["bakery"],
            status: "Active",
            discount: 10
          },
          {
            id: 6,
            name: "Summer Snacks",
            startDate: new Date('2024-05-01'),
            endDate: new Date('2024-05-15'),
            detail: ["foods"],
            status: "Active",
            discount: 5
          },
          {
            id: 7,
            name: "Back-to-School Treats",
            startDate: new Date('2024-08-01'),
            endDate: new Date('2024-08-15'),
            detail: ["drink", "bakery"],
            status: "Active",
            discount: 5
          },
          {
            id: 8,
            name: "Autumn Feast",
            startDate: new Date('2024-09-01'),
            endDate: new Date('2024-09-30'),
            detail: ["foods"],
            status: "Active",
            discount: 5
          },
          {
            id: 9,
            name: "Halloween Special",
            startDate: new Date('2024-10-15'),
            endDate: new Date('2024-10-31'),
            detail: ["drink", "foods"],
            status: "Inactive",
            discount: 20
          },
          {
            id: 10,
            name: "Festive Bites",
            startDate: new Date('2024-11-15'),
            endDate: new Date('2024-11-30'),
            detail: ["bakery"],
            status: "Active",
            discount: 5
          },
          {
            id: 11,
            name: "New Year Cheers",
            startDate: new Date('2024-12-27'),
            endDate: new Date('2025-01-14'),
            detail: ["drink"],
            status: "Active",
            discount: 5
          },
          {
            id: 12,
            name: "Winter Warmth",
            startDate: new Date('2025-01-15'),
            endDate: new Date('2025-02-01'),
            detail: ["bakery", "foods"],
            status: "Active",
            discount: 5
          },
          {
            id: 13,
            name: "Valentine's Treat",
            startDate: new Date('2025-02-01'),
            endDate: new Date('2025-02-14'),
            detail: ["drink"],
            status: "Inactive",
            discount: 10
          },
          {
            id: 14,
            name: "Spring Delicacies",
            startDate: new Date('2025-03-01'),
            endDate: new Date('2025-03-15'),
            detail: ["foods"],
            status: "Active",
            discount: 5
          },
          {
            id: 15,
            name: "Easter Extravaganza",
            startDate: new Date('2025-04-01'),
            endDate: new Date('2025-04-10'),
            detail: ["drink", "bakery"],
            status: "Active",
            discount: 5
          }
        
      ])
      const header = [{
        title: 'id',
        key: 'id',
        value: 'id'
      },
      {
        title: 'Name',
        key: 'name',
        value: 'name'
      },
      {
        title: 'Discount',
        key: 'discount',
        value: 'discount'
      },
    
    
      { title: 'Select', key: 'actions', sortable: false }
    
      ]

      function showPromotion(){
        promotionDialog.value = true
        promotions.value
      }
      

  return { promotions,showPromotion,promotionDialog,header}
})
