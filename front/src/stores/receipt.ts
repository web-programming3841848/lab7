import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useUserStore } from './user'
import { useCustomerStore } from './customer'

export const useReceiptStore = defineStore('receipt', () => {
  const userStore = useUserStore()
  const memberStore = useCustomerStore()
  const receiptDialog = ref(false)
  const receipt = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    totalBefore: 0,
    customerDiscount: 0,
    total: 0,
    receivedAmount: 0,
    change: 0,
    PaymentType: '',
    userId: userStore.currentUser!.id,
    user: userStore.currentUser!.user,
    memberId: 0,
    PromotionDiscount: 0,
    PointDiscount: 0,
    AllDiscount: 0
  })
  const receiptItems = ref<ReceiptItem[]>([])


  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.products?.id === product.id
      && item.products.subCategory === product.subCategory
      && item.products.size === product.size
      && item.products.sweetLevel === product.sweetLevel)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    }
    else {
      const newReceiptItem: ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        productsId: product.id,
        products: product
      }
      receiptItems.value.push(newReceiptItem)
      calReceipt()
    }

  }

  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit == 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }

  function calReceipt() {
    let totalBefore = 0
      for (const item of receiptItems.value) {
        totalBefore = totalBefore + (item.price * item.unit)
      }
  
      receipt.value.totalBefore = totalBefore
      if (memberStore.currentCustomer) {
        receipt.value.customerDiscount = receipt.value.totalBefore * 0.05
        receipt.value.total = (receipt.value.totalBefore) - receipt.value.customerDiscount
        receipt.value.total = (receipt.value.total) - receipt.value.PromotionDiscount
        receipt.value.total = (receipt.value.total) - receipt.value.PointDiscount
        if (receipt.value.total<=0) {
          receipt.value.total=0
        }
      }
      else {
        receipt.value.total = (receipt.value.totalBefore) - receipt.value.PromotionDiscount
        if (receipt.value.total<=0) {
          receipt.value.total=0
        }
      }



  }
  function showReceiptDialog() {
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true

  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      customerDiscount: 0,
      total: 0,
      receivedAmount: 0,
      change: 0,
      PaymentType: 'cash',
      userId: userStore.currentUser!.id,
      user: userStore.currentUser?.user,
      memberId: 0,
      PromotionDiscount: 0,
      PointDiscount: 0,
      AllDiscount: 0

    }
    memberStore.clear()
  }

  return {
    dec, inc, removeReceiptItem, addReceiptItem, calReceipt, showReceiptDialog, clear,
    receiptDialog, receiptItems, receipt
  }
})
