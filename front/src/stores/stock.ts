import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'

//import store
import { useMaterialStore } from './material'
import { useUserStore } from './user'
//import type
import type { Stock } from '@/types/Stock'
import type { StockDetail } from '@/types/StockDetail'
import type { StockForSend } from '@/types/StockAddnew'

import type { Material } from '@/types/Material'

import http from '@/services/http'
import stocksService from '@/services/stock'
import { useLoadingStore } from '@/stores/loading'

import stockService from '@/services/stock'

export const useStockStore = defineStore('stockStore', () => {
  //set name of store
  const userStore = useUserStore()
  const materialStore = useMaterialStore()
  const loadingStore = useLoadingStore()

  //set variable Dialog
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const editItemDialog = ref(false)
  const addNewMaterialDialog = ref(false)
  const StockDialog = ref(false)
  const saveMaterialDialog = ref(false)
  const addNewStockItemDialog = ref(false)
  const addSomeDialog = ref(false)
  const qoh = ref(0)
  const loading = ref(false)

  const initStock = ref<Stock>({
    id: -1,
    date: '',
    userID: -1
  })

  const stocks = ref<Stock[]>([])

  const editedStock = ref<Stock>(initStock.value)

  const initStockItem: StockDetail = {
    id: -1,
    QOH: 0,
    balance: 0,
    materialId: -1,
    StockId: -1
  }

  const stockItems = ref<StockDetail[]>([])
  const editStockItem = ref<StockDetail>(JSON.parse(JSON.stringify(initStockItem)))

  //for get value for ui to set new material
  const name = ref('')
  const minimum = ref(0)
  const typ = ref('')

  const addItem = (material: Material) => {
    console.log(material)
    const index = stockItems.value.findIndex((item) => item.materialId === material.id)
    console.log(index)
    if (index >= 0) {
      stockItems.value[index].QOH++
      return
    } else {
      const newItem: StockDetail = {
        id: stockItems.value.length + 1,
        QOH: qoh.value,
        balance: qoh.value - material.minimum,
        materialId: material.id,
        StockId: -1
      }

      nextTick(() => {
        stockItems.value.push(newItem)
        qoh.value = 0
      })
    }
  }

  function initializeItem() {
    stockItems.value = []
  }

  function getMaterialName(matrialId: any) {
    const material = materialStore.materials.find((mat) => mat && mat.id === matrialId)
    return material ? material.name : 'Unknown Material'
    console.log(material)
  }

  //updateQOHItems and creatStockItemPaper
  // function updateQOHItems() {
  //   //updateQOH and call Balance
  //   addNewStockItemDialog.value = true
  //   for (const item of stockItems.value) {
  //     const updatedItem = stockItems.value.find((i) => i.id === item.id)
  //     if (updatedItem) {
  //       updatedItem.QOH = item.QOH
  //       minimum.value =
  //         materialStore.materials.find((mat) => mat.id === item.materialId)?.minimum ?? 0
  //       updatedItem.balance = updatedItem.QOH - minimum.value

  //       const material = materialStore.materials.find((mat) => mat.id === item.materialId)
  //       if (material) {
  //         material.amount = item.QOH
  //       }
  //     }
  //     console.log(stock.value)
  //   }

  //   //creatStockItemPaper
  //   stock.value.id = stocks.value.length + 1
  //   stock.value.date = new Date()
  //   console.log('User', userStore.currentUser?.id ?? 0)
  //   stock.value.userID = userStore.currentUser?.id ?? 0
  //   stock.value.items = stockItems.value
  //   stocks.value.push(stock.value)
  //   console.log('Saving changes...', stockItems.value)
  //   stock.value = JSON.parse(JSON.stringify(stock))
  //   stockItems.value = []
  //   addNewStockItemDialog.value = false
  // }

  //addnewMaterial item by StockDetail
  function addNewMaterial() {
    console.log('name = ', name.value)
    console.log('manimum = ', minimum.value)
    console.log('type = ', typ.value)

    console.log('new save')
    console.log(materialStore.materials.length)
    const matID = materialStore.materials.length
    editStockItem.value.id = stockItems.value.length
    editStockItem.value.materialId = matID + 1
    materialStore.editMaterial.id = matID + 1
    materialStore.editMaterial.minimum = minimum.value
    materialStore.editMaterial.name = name.value
    materialStore.editMaterial.amount = editStockItem.value.QOH
    materialStore.editMaterial.type = typ.value

    console.log(editStockItem.value)
    console.log('material' + materialStore.editMaterial)

    stockItems.value.push(editStockItem.value)
    materialStore.materials.push(materialStore.editMaterial)
  }

  //stock

  function getUserName(userID: number) {
    const user = userStore.users.find((user) => user && user.id === userID)
    return user ? user.fullName : 'Unknown User'
    console.log(user)
  }

  function getMinimum(materialId: any) {
    const material = materialStore.materials.find((mat) => mat && mat.id === materialId)
    return material ? material.minimum : null
    console.log(material)
  }

  function formatDate(date: Date) {
    const year = date.getFullYear()
    const month = String(date.getMonth() + 1).padStart(2, '0')
    const day = String(date.getDate()).padStart(2, '0')
    return `${year}-${month}-${day}`
  }
  function close() {
    stockItems.value = []
    addNewStockItemDialog.value = false
    addSomeDialog.value = false
  }

  async function getStocks() {
    loadingStore.doLoad()
    const res = await stockService.getStocks()
    stocks.value = res.data
    loadingStore.finish()
  }

  function sendToDelete(stock: Stock) {
    editedStock.value = stock
    dialogDelete.value = true
  }

  async function deleteItemConfirm(stock: Stock) {
    await deleteStock(stock)
    closeDelete()
  }

  async function deleteStock(stock: Stock) {
    loadingStore.doLoad()
    const res = await stocksService.delStock(stock)
    await getStocks()
    loadingStore.finish()
  }

  function closeDelete() {
    dialogDelete.value = false
    editedStock.value = initStock.value
  }

  function editStock(item: Stock) {
    editedStock.value = Object.assign({}, item)
    dialog.value = true
  }

  async function saveStock(stock: Stock) {
    loadingStore.doLoad()
    if (stock.id < 0) {
      //Add new
      const stockForSend = ref<StockForSend>({
        date: stock.date,
        userID: stock.userID
      })
      const res = await stocksService.addStock(stockForSend.value)
    } else {
      //Update
      const res = await stocksService.updateStock(stock)
    }
    getStocks()
    loadingStore.finish()
  }

  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedStock.value = Object.assign({}, initStock.value)
    })
  }

  return {
    stockItems,
    dialog,
    dialogDelete,
    editStockItem,
    deleteItemConfirm,
    editStock,
    deleteStock,
    closeDelete,
    initStockItem,
    initializeItem,
    editItemDialog,
    getMaterialName,
    //updateQOHItems,
    addNewMaterial,
    typ,
    minimum,
    name,
    addNewMaterialDialog,
    saveMaterialDialog,
    stocks,
    getUserName,
    formatDate,
    getMinimum,
    StockDialog,
    addNewStockItemDialog,
    addSomeDialog,
    qoh,
    addItem,
    close,
    editedStock,
    getStocks,
    saveStock,
    closeDialog,
    loading,
    sendToDelete
  }
})
