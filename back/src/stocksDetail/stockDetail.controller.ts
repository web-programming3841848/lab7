import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { StockDetailService } from './stockDetail.service';
import { CreateStockDetailDto } from './dto/create-stockDetail.dto';
import { UpdateStockDetailDto } from './dto/update-stockDetail.dto';

@Controller('stocksDetail')
export class StockDetailController {
  constructor(private readonly stockDetailService: StockDetailService) {}

  //Create
  @Post()
  create(@Body() createUserDto: CreateStockDetailDto) {
    return this.stockDetailService.create(createUserDto);
  }

  //Read All Get All
  @Get()
  findAll() {
    return this.stockDetailService.findAll();
  }

  @Get(':StockId')
  findAllByStockId(@Param('StockId') id: string) {
    return this.stockDetailService.findAllByStockId(+id);
  }

  //Read One Get One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.stockDetailService.findOne(+id);
  }

  //Partial Update
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateStockDto: UpdateStockDetailDto,
  ) {
    return this.stockDetailService.update(+id, updateStockDto);
  }

  //Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.stockDetailService.remove(+id);
  }
}
