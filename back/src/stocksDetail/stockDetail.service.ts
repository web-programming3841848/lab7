import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateStockDetailDto } from './dto/create-stockDetail.dto';
import { UpdateStockDetailDto } from './dto/update-stockDetail.dto';
import { StockDetail } from './entities/stockDetail.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StockDetailService {
  constructor(
    @InjectRepository(StockDetail)
    private stocksRepository: Repository<StockDetail>,
  ) {}
  create(createStockDto: CreateStockDetailDto): Promise<StockDetail> {
    return this.stocksRepository.save(createStockDto);
  }

  findAll(): Promise<StockDetail[]> {
    return this.stocksRepository.find();
  }

  findAllByStockId(stockId: number): Promise<StockDetail[]> {
    return this.stocksRepository.find({ where: { StockId: stockId } });
  }

  findOne(id: number): Promise<StockDetail> {
    return this.stocksRepository.findOneBy({ id: id });
  }

  async update(id: number, updateStockDto: UpdateStockDetailDto) {
    await this.stocksRepository.update(id, updateStockDto);
    const stockDetail = await this.stocksRepository.findOneBy({ id: id });
    return stockDetail;
  }

  async remove(id: number): Promise<StockDetail> {
    const delStockDetail = await this.stocksRepository.findOneBy({ id: id });
    return this.stocksRepository.remove(delStockDetail);
  }
}
