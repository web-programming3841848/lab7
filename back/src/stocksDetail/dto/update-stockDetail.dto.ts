import { PartialType } from '@nestjs/swagger';
import { CreateStockDetailDto } from './create-stockDetail.dto';

export class UpdateStockDetailDto extends PartialType(CreateStockDetailDto) {}
