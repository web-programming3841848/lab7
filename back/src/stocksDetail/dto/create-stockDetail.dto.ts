import { IsString, IsNotEmpty, IsNumber } from 'class-validator';

export class CreateStockDetailDto {
  @IsNotEmpty()
  QOH: number;

  @IsNotEmpty()
  balance: number;

  @IsNumber()
  materialId: number;

  @IsNumber()
  StockId: number;
}
