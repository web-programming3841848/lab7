import { Module } from '@nestjs/common';
import { StockDetailService } from './stockDetail.service';
import { StockDetailController } from './stockDetail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StockDetail } from './entities/stockDetail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StockDetail])],
  controllers: [StockDetailController],
  providers: [StockDetailService],
})
export class StockDetailModule {}
