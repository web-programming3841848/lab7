import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class StockDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  QOH: number;

  @Column()
  balance: number;

  @Column()
  materialId: number;

  @Column()
  StockId: number;
}
