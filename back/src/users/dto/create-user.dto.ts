export class CreateUserDto {
  fullName: string;
  email: string;
  tel: string;
  address: string;
  rank: string;
  status: 'Active' | 'Inactive';
  password: string;
  user: string;
}
