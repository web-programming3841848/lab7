import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemparatureModule } from '../Temparature/temparature.module';
import { UsersModule } from '../users/users.module';
import { StocksModule } from '../stocks/stocks.module';
import { StockDetailModule } from '../stocksDetail/stockDetail.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { Stock } from '../stocks/entities/stock.entity';
import { StockDetail } from '../stocksDetail/entities/stockDetail.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',
      entities: [Stock, StockDetail],
      synchronize: true,
    }),
    TemparatureModule,
    UsersModule,
    StocksModule,
    StockDetailModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
