import { Test, TestingModule } from '@nestjs/testing';
import { StocksController } from './stock.controller';
import { StocksService } from './stocks.service';

describe('StockController', () => {
  let controller: StocksController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StocksController],
      providers: [StocksService],
    }).compile();

    controller = module.get<StocksController>(StocksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
