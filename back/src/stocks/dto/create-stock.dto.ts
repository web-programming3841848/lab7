import { IsString, IsNotEmpty, IsNumber } from 'class-validator';

export class CreateStockDto {
  @IsString()
  @IsNotEmpty()
  date: string;

  @IsNumber()
  userID: number;
}
