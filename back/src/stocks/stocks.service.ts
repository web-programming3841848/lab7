import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Stock } from './entities/stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(Stock)
    private stocksRepository: Repository<Stock>,
  ) {}
  create(createStockDto: CreateStockDto): Promise<Stock> {
    return this.stocksRepository.save(createStockDto);
  }

  findAll(): Promise<Stock[]> {
    return this.stocksRepository.find();
  }

  findOne(id: number): Promise<Stock> {
    return this.stocksRepository.findOneBy({ id: id });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    await this.stocksRepository.update(id, updateStockDto);
    const stock = await this.stocksRepository.findOneBy({ id: id });
    return stock;
  }

  async remove(id: number): Promise<Stock> {
    const delStock = await this.stocksRepository.findOneBy({ id: id });
    return this.stocksRepository.remove(delStock);
  }
}
